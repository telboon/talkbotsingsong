#!/usr/bin/env python3

import telepot
import time
import sys
from telepot.loop import MessageLoop
from telepot.delegate import pave_event_space, per_chat_id, create_open
from yahoo_finance import Share
import botfunctions
import json
import os

class MessageHandler(telepot.helper.ChatHandler):
   def __init__(self, *args, **kwargs):
      super(MessageHandler, self).__init__(*args, **kwargs)
      
      #Get all member history
      running=0
      self.chatList=[]
      self.memberList=[]
      priorChat=""
      with open("memberList.txt") as memberFile:
         for line in memberFile:
            line=line[0:-1]
            running+=1
            if running%2==1:
               priorChat=line
            elif running%2==0:
               if priorChat in self.chatList:
                  #add member to chatlist
                  self.memberList[self.chatList.index(priorChat)].append(line)
               else:
                  #create new chat list
                  self.chatList.append(priorChat)
                  self.memberList.append([line])
      #done reading memberList

      #read chatLogs
      with open("chatLog.json") as chatLogFile:
         self.chatLog=json.load(chatLogFile)
      #done reading chatLogs

   def on_chat_message(self,msg):
      #if existing chat
      if str(str(msg["chat"]["id"])) in self.chatList:
         #check if user has been registered in existing chat
         if not (str(msg["from"]["id"]) in self.memberList[self.chatList.index(str(msg["chat"]["id"]))]):
            self.memberList[self.chatList.index(str(msg["chat"]["id"]))].append(str(msg["from"]["id"]))

            with open("memberList.txt","a") as memberFile:
               memberFile.write(str(msg["chat"]["id"])+"\n"+str(msg["from"]["id"])+"\n")

            print("New user("+str(msg["from"]["id"])+") identified in "+str(msg["chat"]["id"]))

      #if new chat
      else:
            self.chatList.append(str(msg["chat"]["id"]))
            self.memberList.append([str(msg["from"]["id"])])

            with open("memberList.txt","a") as memberFile:
               memberFile.write(str(msg["chat"]["id"])+"\n"+str(msg["from"]["id"])+"\n")

            print("New user("+str(msg["from"]["id"])+") identified in new chat("+str(msg["chat"]["id"])+")")
      
      ################################# Keep Chat Log ################################
      currRecord={"chat_id":str(msg["chat"]["id"]), "user_id": str(msg["from"]["id"]), "time":msg["date"], "msg":msg["text"]}
      self.chatLog.update({len(self.chatLog): currRecord})
      with open("chatLog.json","w") as chatLogFile:
         json.dump(self.chatLog,chatLogFile)
      ################################# Keep Chat Log ################################

      #start working
      workingText=msg["text"]
      workingChat=msg["chat"]["id"]
      workingFrom=msg["from"]["id"]
      workingName=msg["from"]["first_name"]
      print(workingName+": "+workingText)

      ################################## Botting Functions ################################
      #start instructions
      if workingText=="/start":
         self.sender.sendMessage(botfunctions.start(workingChat, workingFrom, workingText, workingName),parse_mode="html",disable_web_page_preview=True)

      #record instructions
      elif workingText[:7]=="/record":
         self.sender.sendMessage(botfunctions.record(workingChat, workingFrom, workingText, workingName),parse_mode="html",disable_web_page_preview=True)

      #erase instructions
      elif workingText=="/erase":
         self.sender.sendMessage(botfunctions.erase(workingChat, workingFrom, workingText, workingName),parse_mode="html",disable_web_page_preview=True)

      #remember instructions
      elif workingText=="/remember":
         self.sender.sendMessage(botfunctions.remember(workingChat, workingFrom, workingText, workingName),parse_mode="html",disable_web_page_preview=True)

      #news instructions
      elif workingText=="/news":
         self.sender.sendMessage(botfunctions.news(workingChat, workingFrom, workingText, workingName),parse_mode="html",disable_web_page_preview=True)

      #joke instructions
      elif workingText=="/joke":
         self.sender.sendMessage(botfunctions.joke(workingChat, workingFrom, workingText, workingName),parse_mode="html",disable_web_page_preview=True)

      #stock instruction
      elif workingText[:6]=="/stock":
         self.sender.sendMessage(botfunctions.stock(workingChat, workingFrom, workingText, workingName)[0],parse_mode="html",disable_web_page_preview=True)

      #lyrics instruction
      elif workingText[:7]=="/lyrics":
         self.sender.sendMessage(botfunctions.lyrics(workingChat, workingFrom, workingText, workingName),parse_mode="html",disable_web_page_preview=True)

      #watchlist instruction
      elif workingText[:10]=="/watchlist":
         self.sender.sendMessage(botfunctions.watchlist(workingChat, workingFrom, workingText, workingName),parse_mode="html",disable_web_page_preview=True)

      #news for specific stock instruction
      elif workingText[:5]=="/news":
         newsInfo, newsTicker=botfunctions.stock(workingChat, workingFrom, "/stock "+workingText[6:], workingName)
         self.sender.sendMessage(newsInfo,parse_mode="html",disable_web_page_preview=True)
         self.sender.sendMessage(botfunctions.stockNews(workingChat, workingFrom, "/news "+newsTicker, workingName), parse_mode="html", disable_web_page_preview=True)

      #graph for specific stock instruction
      elif workingText[:6]=="/graph":
         graphInfo, graphTicker=botfunctions.stock(workingChat, workingFrom, "/stock "+workingText[7:], workingName)
         self.sender.sendMessage(graphInfo,parse_mode="html",disable_web_page_preview=True)
         graphTicker=graphTicker.upper()

         curr_path=os.getcwd()
         os.chdir("samFinance/")
         photoFilePath=botfunctions.stockGraph(workingChat, workingFrom, "/graph "+graphTicker, workingName)
         photoFile=open(photoFilePath, "rb")
         self.sender.sendPhoto(photoFile)
         photoFile.close()
         os.chdir(curr_path)



      ################################## Botting Functions ################################

token=""
bot=telepot.Bot(token)
print("TalkBotSingSong starting...\n"+str(bot.getMe()))

bot=""
bot=telepot.DelegatorBot(token,[pave_event_space()(per_chat_id(), create_open, MessageHandler,timeout=300),])

MessageLoop(bot).run_as_thread()

while 1:
   time.sleep(10)

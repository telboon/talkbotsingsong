import feedparser
from pathlib import Path
import random
import json
import time
import requests
import csv
import google
from bs4 import BeautifulSoup
import datetime
import samFinance.samFinance as samFinance

#start instructions
def start(workingChat, workingFrom, workingText, workingName):
   welcomeText="Hello. I'm a talk bot that sings song too. I currently have the below functions:\n"
   welcomeText+="/news - Shows selected latest news"+"\n"
   welcomeText+="/stock &lt;stock ticker&gt; - Shows basic stock data"+"\n"
   welcomeText+="/graph &lt;stock ticker&gt; - Shows historical price & distribution"+"\n"
   welcomeText+="/watchlist - Stocks watchlist module"+"\n"
   welcomeText+="/joke - That's you! Hah!"+"\n"
   welcomeText+="/lyrics &lt;song name&gt; - Search for song lyrics"+"\n"

   return welcomeText

#record instructions
def record(workingChat, workingFrom, workingText, workingName):
   workingText=workingText[8:]
   testFile=Path("./record/"+str(workingChat)+"."+str(workingFrom))
   if testFile.exists():
      with open("./record/"+str(workingChat)+"."+str(workingFrom),"a") as recordFile:
         recordFile.write(workingText+"\n")
   else:
      with open("./record/"+str(workingChat)+"."+str(workingFrom),"w") as recordFile:
         recordFile.write(workingText+"\n")
   return "I have recorded your message:\n"+workingText

#erase instructions
def erase(workingChat, workingFrom, workingText, workingName):
   with open("./record/"+str(workingChat)+"."+str(workingFrom),"w") as recordFile:
      recordFile.write("")
   return "Records erased"

#remember instructions
def remember(workingChat, workingFrom, workingText, workingName):
   testFile=Path("./record/"+str(workingChat)+"."+str(workingFrom))
   if testFile.exists():
      readOut=""
      with open("./record/"+str(workingChat)+"."+str(workingFrom)) as recordFile:
         for line in recordFile:
            readOut+=line
      return readOut
   else:
      return "No record found"

#news instructions
def news(workingChat, workingFrom, workingText, workingName):
   feed1=feedparser.parse("https://www.reddit.com/r/worldnews/.rss")
   feed2=feedparser.parse("http://www.asiaone.com/rss-feed/2")
   feed3=feedparser.parse("http://www.asiaone.com/rss-feed/11")
   newsText="<b>These are the following latest news:</b>\n"
   newsText+=" - <a href=\""+feed1["items"][0]["link"]+"\">"+feed1["items"][0]["title"]+"</a>\n"
   newsText+=" - <a href=\""+feed1["items"][1]["link"]+"\">"+feed1["items"][1]["title"]+"</a>\n"
   newsText+=" - <a href=\""+feed1["items"][2]["link"]+"\">"+feed1["items"][2]["title"]+"</a>\n"
   newsText+=" - <a href=\""+feed2["items"][0]["link"]+"\">"+feed2["items"][0]["title"]+"</a>\n"
   newsText+=" - <a href=\""+feed2["items"][1]["link"]+"\">"+feed2["items"][1]["title"]+"</a>\n"
   newsText+=" - <a href=\""+feed2["items"][2]["link"]+"\">"+feed2["items"][2]["title"]+"</a>\n"
   newsText+=" - <a href=\""+feed3["items"][0]["link"]+"\">"+feed3["items"][0]["title"]+"</a>\n"
   newsText+=" - <a href=\""+feed3["items"][1]["link"]+"\">"+feed3["items"][1]["title"]+"</a>\n"
   return newsText

#joke instructions
def joke(workingChat, workingFrom, workingText, workingName):
   feed1=feedparser.parse("https://www.reddit.com/r/jokes/.rss")
   jokeID=random.randrange(0,len(feed1["items"]))
   joke=feed1["items"][jokeID]["title"]+"\n"+feed1["items"][jokeID]["summary"]
   joke=joke[:joke.index("<!-- SC_ON")]
   while ("<" in joke) and (">" in joke):
      startTag=joke.index("<")
      endTag=joke.index(">")
      joke=joke[:startTag]+joke[endTag+1:]
   return joke

#tags remover:
def tagRemover(htmlStr):
   while "<" in htmlStr:
      htmlStr=htmlStr[:htmlStr.index("<")]+htmlStr[htmlStr.index(">",htmlStr.index("<"))+1:]
   return htmlStr

#stock instruction
def stock(workingChat, workingFrom, workingText, workingName):
   stockNotExist=False
   workingText=workingText[7:]
   #check if Singapore stock first (n: name; l: last trade; j: 52 week low; k: 52 week high; j1: market cap; d: dividend per share; y: Dividend Yield; r: pe; b4: book value; p6: price to book)
   stockResult=str(requests.get("http://finance.yahoo.com/d/quotes.csv?s="+workingText+".SI"+"&f=nljkj1dyrb4p6").content)[2:-3]
   if stockResult[:3]=="N/A":
      stockResult=str(requests.get("http://finance.yahoo.com/d/quotes.csv?s="+workingText+"&f=nljkj1dyrb4p6").content)[2:-3]
      #check if stock exists at all
      if stockResult[:3]=="N/A":
         #search for stock in yahoo japan
         japanResult=requests.get("http://stocks.finance.yahoo.co.jp/stocks/detail/?code="+workingText+".T").content.decode("utf-8")
         if not ("一致する銘柄は見つかりませんでした" in japanResult):
            workingTicker=workingText
            japanSoup=BeautifulSoup(japanResult,"html.parser")
            stockResult=""
            stockResult+="\""+tagRemover(str(japanSoup.find_all("th",{"class":"symbol"})[0]))+"\","
            stockResult+="\""+tagRemover(str(japanSoup.find_all("td",{"class":"stoksPrice"})[1]))+"\","
            stockResult+="\""+tagRemover(str(japanSoup.find_all("dd",{"class":"ymuiEditLink mar0"})[18]))+"\","
            stockResult+="\""+tagRemover(str(japanSoup.find_all("dd",{"class":"ymuiEditLink mar0"})[17]))+"\","
            stockResult+="\""+tagRemover(str(japanSoup.find_all("dd",{"class":"ymuiEditLink mar0"})[7]))+"\","
            stockResult+="\""+tagRemover(str(japanSoup.find_all("dd",{"class":"ymuiEditLink mar0"})[10]))+"\","
            stockResult+="\""+tagRemover(str(japanSoup.find_all("dd",{"class":"ymuiEditLink mar0"})[9]))+"\","
            stockResult+="\""+tagRemover(str(japanSoup.find_all("dd",{"class":"ymuiEditLink mar0"})[11]))+"\","
            stockResult+="\""+tagRemover(str(japanSoup.find_all("dd",{"class":"ymuiEditLink mar0"})[14]))+"\","
            stockResult+="\""+tagRemover(str(japanSoup.find_all("dd",{"class":"ymuiEditLink mar0"})[12]))+"\","
         else:
            urlGenerator=google.search("site:finance.yahoo.com "+workingText,stop=9)
            googleCount=0
            #search in google for ticker
            try:
               url=next(urlGenerator)
               while not ("finance.yahoo.com/quote/"in url) and googleCount<5:
                  googleCount+=1
                  url=next(urlGenerator)
               if "finance.yahoo.com/quote/" in url:
                  workingText=url[url.index("finance.yahoo.com/quote/")+24:]
                  #bunch of clearing up the url
                  if "/" in workingText:
                     workingText=workingText[:workingText.index("/")]
                  if "?" in workingText:
                     workingText=workingText[:workingText.index("?")]
                  stockResult=str(requests.get("http://finance.yahoo.com/d/quotes.csv?s="+workingText+"&f=nljkj1dyrb4p6").content)[2:-3]
                  workingTicker=workingText
               else:
                  stockNotExist=True
                  return workingText+" not found. Please refine your company name", "N/A"
            except:
               return workingText+" not found. Please refine your company name", "N/A"
      else:
         workingTicker=workingText
   else:
      workingTicker=workingText+".SI"
   
   if stockResult=="":
      return workingText+" not found. Please refine your company name", "N/A"
   #start parsing data if stock exist
   if not stockNotExist:
      stockParse=list(csv.reader([stockResult]))
      stockMessage=""
      #name
      stockMessage+="<b>"
      stockMessage+=stockParse[0][0]
      stockMessage+="</b>"
      stockMessage+=" ("+workingTicker+")\n"
      #Last Trade
      stockMessage+="<b>Last Trade: </b>"
      stockMessage+=stockParse[0][1]+"\n"
      #52 weeks range
      stockMessage+="<b>52 Week Price Range: </b>"
      stockMessage+=stockParse[0][2]+" "
      stockMessage+=stockParse[0][3]+"\n"
      #Market Cap
      stockMessage+="<b>Market Cap: </b>"
      stockMessage+=stockParse[0][4]+"\n"
      #Dividend
      stockMessage+="<b>Dividend: </b>"
      stockMessage+=stockParse[0][5]+" ("
      stockMessage+=stockParse[0][6]+"%)\n"
      #PE Ratio
      stockMessage+="<b>PE Ratio: </b>"
      stockMessage+=stockParse[0][7]+"\n"
      #Book Value
      stockMessage+="<b>Book Value: </b>"
      stockMessage+=stockParse[0][8]+"\n"
      #Price/Book
      stockMessage+="<b>Price to Book: </b>"
      stockMessage+=stockParse[0][9]+"\n"
      #Send Message Finally
      return stockMessage, workingTicker

#lyrics instruction
def lyrics(workingChat, workingFrom, workingText, workingName):
   workingText=workingText[8:]
   urlGenerator=google.search("site:lyricsfreak.com "+workingText, stop=9)
   url=next(urlGenerator)
   musicSite=requests.get(url).content
   musicSoup=BeautifulSoup(musicSite,"html.parser")
   try:
      songLyrics=str(musicSoup.findAll("div", {"id": "content_h"})[0])
      ###########         clean up of lyrics  ########################
      while "<div" in songLyrics:
         songLyrics=songLyrics[:songLyrics.index("<div")]+songLyrics[songLyrics.index(">",songLyrics.index("<div"))+1:]
         while "</div" in songLyrics:
            songLyrics=songLyrics[:songLyrics.index("</div")]+songLyrics[songLyrics.index(">",songLyrics.index("</div"))+1:]
      while "<br>" in songLyrics:
         songLyrics=songLyrics[:songLyrics.index("<br>")]+"\n"+songLyrics[songLyrics.index("<br>")+4:]
      while "<br/>" in songLyrics:
         songLyrics=songLyrics[:songLyrics.index("<br/>")]+"\n"+songLyrics[songLyrics.index("<br/>")+5:]
      ###########         clean up of lyrics  ########################
      songLyrics="<b>"+musicSoup.title.string+"</b>\n"+songLyrics
      return songLyrics
   except:
      return "Your song requested is not found. Please type in more details of the song for better result."

#watchlist load and save file
def watchLoadFile(workingChat, workingFrom):
   stockList=[]
   try:
      with open("./watchlist/"+str(workingChat)+"."+str(workingFrom)) as recordFile:
         for line in recordFile:
            while "\n" in line:
               line=line[:line.index("\n")]
            stockList.append(line)
   except:
      print("Read watchlist file failure")
   return list(set(stockList))

def watchSaveFile(stockList,workingChat, workingFrom):
   stockList=list(set(stockList))
   with open("./watchlist/"+str(workingChat)+"."+str(workingFrom),"w") as recordFile:
      for i in range(len(stockList)):
         recordFile.write(stockList[i]+"\n")
   return True


#watchlist instruction
def watchlist(workingChat, workingFrom, workingText, workingName):
   if workingText=="/watchlist":
      watchlistIntro="This is the watchlist module of the chat bot. Please use the following functions:\n"
      watchlistIntro+="/watchlist add &lt;ticker&gt; - Adds a new stock to the watchlist\n"
      watchlistIntro+="/watchlist remove &lt;ticker&gt; - Removes an existing stock from the watchlist\n"
      watchlistIntro+="/watchlist show - Show components of watchlist\n"
      watchlistIntro+="/watchlist update - Update prices for watchlist\n"
      return watchlistIntro

   # Parsing additional functions
   workingText=workingText[11:]

   if workingText[:3]=="add":
      workingText=workingText[4:]
      newResults, newTicker=stock(workingChat, workingFrom, "/stock "+workingText, workingName)
      if newTicker!="N/A":
         newResults="New ticker added to "+workingName+" watchlist\n"+newResults

         #check if file exists and put new ticker
         testFile=Path("./watchlist/"+str(workingChat)+"."+str(workingFrom))
         if testFile.exists():
            with open("./watchlist/"+str(workingChat)+"."+str(workingFrom),"a") as recordFile:
               recordFile.write(newTicker.upper()+"\n")
         else:
            with open("./watchlist/"+str(workingChat)+"."+str(workingFrom),"w") as recordFile:
               recordFile.write(newTicker.upper()+"\n")

         #double confirm remove duplicate
         watchSaveFile(watchLoadFile(workingChat,workingFrom),workingChat,workingFrom)

         return newResults
      else:
         return "Invalid ticker added. Pleaes try again."

   elif workingText[:6]=="remove":
      workingText=workingText[6:]
      newResults, newTicker=stock(workingChat, workingFrom, "/stock "+workingText, workingName)

      if newTicker!="N/A":
         #load from file
         stockList=watchLoadFile(workingChat, workingFrom)
         
         #remove from list
         watchlistRemove=False
         while newTicker in stockList:
            watchlistRemove=True
            stockList.pop(stockList.index(newTicker))

         watchSaveFile(list(set(stockList)),workingChat, workingFrom)
         if watchlistRemove:
            return newTicker+" removed from "+workingName+" watchlist"
         else:
            return newTicker+" was not found in "+workingName+" watchlist"
      else:
         return "Invalid ticker. Please try again."
   
   elif workingText[:6]=="update":
      #load from file
      stockList=watchLoadFile(workingChat, workingFrom)
      fullReport="<b>Watchlist report for "+workingName+"</b>\n"

      for i in range(len(stockList)):
         newResults, newTicker=stock(workingChat, workingFrom, "/stock "+stockList[i], workingName)
         fullReport+=newResults+"\n"
      return fullReport

   elif workingText[:4]=="show":
      #load from file
      stockList=watchLoadFile(workingChat, workingFrom)
      fullReport="<b>Watchlist for "+workingName+"</b>\n"
      for i in range(len(stockList)):
         fullReport+=stockList[i]+"\n"

      return fullReport

   else:
      return "Invalid watchlist entry. Please try again."

#stockNews instructions
def stockNews(workingChat, workingFrom, workingText, workingName):
   workingText=workingText[6:]

   #reading RSS for news
   newsFeed=feedparser.parse("http://finance.yahoo.com/rss/headline?s="+workingText)
   newsText="<b>Related News for "+workingText+"</b>\n"
   for i in range(5):
      try:
         newsText+=" - <a href=\""+newsFeed["items"][i]["link"]+"\">"+newsFeed["items"][i]["title"]+"</a>\n"
      except:
         pass
   return newsText

#stockGraph instructions
def stockGraph(workingChat, workingFrom, workingText, workingName):
   workingText=workingText[7:]
   startDate=datetime.datetime(2016,1,1)
   
   #start triggering samFinance
   myStock=samFinance.stock(workingText)
   try:
      myStock.update(1, 5)
      myStock.save()
   except:
      pass

   #start charting
   samFinance.algo.plotPriceChart(myStock, startDate=startDate)

   return "graphs/"+workingText+".png"
